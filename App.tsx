import React, {useState, useEffect} from 'react';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {
  Text,
  View,
  Button,
  StatusBar,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import {storage} from './Storage';

function App(): JSX.Element {
  const [isDarkMode, setIsDarkMode] = useState<boolean>(false);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  useEffect(() => {
    const listener = storage.addOnValueChangedListener(changedKey => {
      if (changedKey === 'isDarkMode') {
        const newValue = storage.getBoolean(changedKey);
        console.log('theme:', newValue);
      }
    });

    return () => {
      listener.remove();
    };
  }, []);

  const toggleTheme = () => {
    const newMode = !isDarkMode;
    setIsDarkMode(newMode);
    storage.set('isDarkMode', newMode);
  };

  const storingData = () => {
    storage.set('username', 'Innocent'); // string
    storage.set('age', 25); // number
    storage.set('is-mmkv-fast-asf', true); // boolean

    // objects
    const user = {
      name: 'Chimezie',
      location: 'Nigeria',
      email: 'chimezieinnocent39@gmail.com',
    };
    storage.set('userDetails', JSON.stringify(user));

    // arrays
    const numberArray = [1, 2, 3, 4, 5];
    const serializedArray = JSON.stringify(numberArray);
    storage.set('numbers', serializedArray);
  };

  const retrievingData = () => {
    const username = storage.getString('username'); // 'Innocent'
    const age = storage.getNumber('age'); // 25
    const isMmkvFast = storage.getBoolean('is-mmkv-fast-asf'); // true
    console.group({username: username}, {age: age}, {isMmkvFast: isMmkvFast});

    // objects
    const serializedUser: string | undefined = storage.getString('userDetails');
    if (serializedUser !== undefined) {
      const userObject = JSON.parse(serializedUser);
      console.log('userObject:', userObject);
    } else {
      return;
    }

    // arrays
    const serializedArray: string | undefined = storage.getString('numbers');
    if (serializedArray !== undefined) {
      const numberArray = JSON.parse(serializedArray);
      console.log('numberArray:', numberArray);
    } else {
      return;
    }
  };

  const getAllKeys = () => {
    const allKeys = storage.getAllKeys();
    console.log(allKeys);
  };

  const deleteData = () => {
    storage.delete('username');
    storage.delete('age');
  };

  const deleteAll = () => {
    storage.clearAll();
  };

  const encryptPassword = () => {
    storage.set('userPassword', 'This is a secret user password');

    // Retrieving the password data from the encrypted storage
    const password = storage.getString('userPassword');
    console.log(password);
  };

  return (
    <SafeAreaView style={[backgroundStyle, styles.sectionContainer]}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />

      <View>
        <Text
          style={[
            styles.sectionTitle,
            {
              color: isDarkMode ? Colors.white : Colors.black,
            },
          ]}>
          {isDarkMode ? 'Dark Mode' : 'Light Theme'}
        </Text>
        <Text
          style={[
            styles.sectionDescription,
            {
              color: isDarkMode ? Colors.light : Colors.dark,
            },
          ]}>
          React Native MMKV Tutorial
        </Text>
        <Button
          onPress={toggleTheme}
          title={isDarkMode ? 'Switch to light mode' : 'Switch to dark mode'}
        />
        <Button onPress={storingData} title="Store Data" />
        <Button onPress={retrievingData} title="Retrieve Data" />
        <Button onPress={getAllKeys} title="Get All Keys" />
        <Button onPress={deleteData} title="Delete Data" />
        <Button onPress={deleteAll} title="Delete All Data" />
        <Button onPress={encryptPassword} title="Encrypt Password" />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'center',
  },
  sectionDescription: {
    marginVertical: 8,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
